import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { HeaderStateService } from './service/header-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Fullstack';
  showHead: boolean = false;

  subs;

  constructor(private router: Router, private headerService: HeaderStateService) {

    this.subs = this.headerService.events.subscribe(data => {this.showHead = data;})

      router.events.forEach((event) => {
        if (event instanceof NavigationStart) {
          if (event['url'] == 'login') {
            this.showHead = false;
          } else {
            this.showHead = true;
          }
        }
      });
    }
}
