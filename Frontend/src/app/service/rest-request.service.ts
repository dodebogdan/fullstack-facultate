import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestRequestService {

  constructor(private http: HttpClient) { }

  get(url) {}

  post(url, body): Observable<any> {
    return this.http.post(url, body).pipe(take(1));
  }
}
