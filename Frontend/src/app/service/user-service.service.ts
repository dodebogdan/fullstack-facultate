import { Injectable } from '@angular/core';
import { RestRequestService } from './rest-request.service';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  url: string = "http://localhost:3000/";
  constructor(private restRequest: RestRequestService) { }

  login(email, password) {
    return this.restRequest.post(`${this.url}login`,{ email: email, password:password });
  }
  register(email, password) {
    return this.restRequest.post(`${this.url}register`,{ email: email, password:password });
  }
}
