import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Employee } from '../entities/employee.entity';
import { EmployeesService } from '../service/employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  employeesList: Employee[]
  employeesColumns: string[]

  constructor(private empService: EmployeesService) { }

  ngOnInit(): void {
    this.employeesColumns = this.getHeader()
    this.empService.getEmployees()
      .pipe(tap((result: Employee[]) => this.employeesList = result))
      .subscribe();

  }

  getHeader() {
    return ['surname','age','firstname'
    ];
  }

}
