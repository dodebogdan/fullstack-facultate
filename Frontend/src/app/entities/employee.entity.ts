export class Employee {
    id?: string;
    firstname?: string;
    surname?: string;
    age?: number;

    public constructor(init?: Partial<Employee>) {
        Object.assign(this, init)
    }
}
