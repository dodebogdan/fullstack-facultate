import { Component } from '@angular/core';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.scss'
  ]
})
export class UploadPhotoComponent {

	url: any;
	
	selectFile(event: any) { 
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			return;
		}

		var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    
		reader.onload = (_event) => {
      this.url = reader.result;   
		}
	}

}
