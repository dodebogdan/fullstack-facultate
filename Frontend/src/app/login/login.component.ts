import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HeaderStateService } from '../service/header-state.service';
import { UserServiceService } from '../service/user-service.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('',[Validators.required, Validators.email]),
    password: new FormControl('',[Validators.required, Validators.minLength(8)])
  })
  returnUrl: string;

  constructor(private headerService: HeaderStateService, private userService: UserServiceService,
    private router: Router,private snackBar: MatSnackBar) {
    this.headerService.hide();
   }

  ngOnInit(): void {

  }
  get f() { return this.loginForm.controls; }
  onSubmit() {
    if (this.loginForm.invalid) {
        return;
    }
    this.userService.login(this.f.email.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
              if (data.success ===true) {
                this.openSnackBar(`Successful login.`,true);
                this.router.navigate(['/history']);
              }
              else{
                this.openSnackBar(data.message,false);
              }
            },
            error => {
                // error handling
            });
}
openSnackBar(message, type) {
  if(type===true){
  this.snackBar.open(message, '', {
    duration: 2000,
    panelClass: ['success_snackbar']
  });}
  else{
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['fail_snackbar']
    });
  }
}
}
