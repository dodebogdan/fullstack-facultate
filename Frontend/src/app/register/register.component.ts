import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { HeaderStateService } from '../service/header-state.service';
import { UserServiceService } from '../service/user-service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    email: new FormControl('',[Validators.required, Validators.email]),
    password: new FormControl('',[Validators.required, Validators.minLength(8)])
  })
  returnUrl: string;
  constructor(private headerService: HeaderStateService, private userService: UserServiceService,
    private router: Router,private snackBar: MatSnackBar) { 
      this.headerService.hide();
    }

  ngOnInit(): void {
  }
  get f() { return this.registerForm.controls; }
  onSubmit() {
    if (this.registerForm.invalid) {
        return;
    }
    this.userService.register(this.f.email.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate(['/login']);
            },
            error => {
                // error handling
            });
}
openSnackBar(message, type) {
  if(type===true){
  this.snackBar.open(message, '', {
    duration: 2000,
    panelClass: ['success_snackbar']
  });}
  else{
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['fail_snackbar']
    });
  }
}
}
