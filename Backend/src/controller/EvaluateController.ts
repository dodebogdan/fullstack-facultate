import {NextFunction, Request, Response} from "express";
import { MnistData } from "../data";
import * as tf from '@tensorflow/tfjs-node'

export class EvaluateController {

    data : MnistData

    async evaluate(request: Request, response: Response, next: NextFunction) {
        this.data = new MnistData();
        await this.data.load();
        const model = await tf.loadLayersModel("file://src/resorces/model.json");
        
        let predictionResult = this.doPrediction(model, this.data, 1)
        return response.json({
            preds: predictionResult[0],
            labels: predictionResult[1]
        })

    }

    private doPrediction(model, data, testDataSize = 500) {
        const IMAGE_WIDTH = 28;
        const IMAGE_HEIGHT = 28;
        const testData = data.nextTestBatch(testDataSize);
        const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
        const labels = testData.labels.argMax(-1);
        const preds = model.predict(testxs).argMax(-1);
      
        testxs.dispose();
        return [preds, labels];
    }


}