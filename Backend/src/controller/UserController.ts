import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) { 
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }
    async login(req: Request, res: Response, next: NextFunction) {
        
        let user = await this.userRepository.findOne({ email: req.body.email });
        if (!user) return res.send({success: false, message:'No user found.'});
        if (!(req.body.password === user.password)) return res.send({success: false, message:'Email or password invalid...'});
        res.status(200).send({ success: true});
    }

    async register(req: Request, res: Response, next: NextFunction){
            
            this.save(req,
            res,next); 
    }

}