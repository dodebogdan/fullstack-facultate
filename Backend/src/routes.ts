import {UserController} from "./controller/UserController";
import {EvaluateController} from "./controller/EvaluateController";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one"
}, 
{
    method: "get",
    route: "/evaluate",
    controller: EvaluateController,
    action: "evaluate"
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove"
},
{
    method: "post",
    route: "/register",
    controller: UserController,
    action: "save"
},
{
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login"
}
];