import { Datastore } from '@google-cloud/datastore';
const datastore = new Datastore({
	projectId: 'fullstack-facultate',
	keyFilename: 'fullstack-facultate-218981a38d50.json'
});
const kindName = 'user-log';

export function savelog (req, res){
	let uid = req.query.uid || req.body.uid || 0;
	let log = req.query.log || req.body.log || '';

	datastore
		.save({
			key: datastore.key(kindName),
			data: {
				log: log,
				uid: datastore.int(uid),
				time_create: datastore.int(Math.floor(new Date().getTime()/1000))
			}
		})
		.catch(err => {
		    console.error('ERROR:', err);
		    res.status(200).send(err);
		    return;
		});

	res.status(200).send(log);
};




export function helloWorld(req, res){
    res.send({msg: 'Hello, World'});
};

const kindName2 = 'employee';

export function getEmployees(req, res) {
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET')
  res.set('Access-Control-Allow-Headers', 'Authorization')
  const query = datastore.createQuery(kindName2)
  let employees: any
  datastore.runQuery(query)
    .then(results => {
      employees = results[0];
      console.log(employees)
      return res.status(200).send(employees);
    })
    .catch(error => {
      console.error('ERROR: ', error);
      return res.status(200).send(error);
    });
};
