"use strict";
exports.__esModule = true;
exports.getEmployees = exports.helloWorld = exports.savelog = void 0;
var datastore_1 = require("@google-cloud/datastore");
var datastore = new datastore_1.Datastore({
    projectId: 'fullstack-facultate',
    keyFilename: 'fullstack-facultate-218981a38d50.json'
});
var kindName = 'user-log';
function savelog(req, res) {
    var uid = req.query.uid || req.body.uid || 0;
    var log = req.query.log || req.body.log || '';
    datastore
        .save({
        key: datastore.key(kindName),
        data: {
            log: log,
            uid: datastore.int(uid),
            time_create: datastore.int(Math.floor(new Date().getTime() / 1000))
        }
    })["catch"](function (err) {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
    });
    res.status(200).send(log);
}
exports.savelog = savelog;
;
function helloWorld(req, res) {
    res.send({ msg: 'Hello, World' });
}
exports.helloWorld = helloWorld;
;
var kindName2 = 'employee';
function getEmployees(req, res) {
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Authorization');
    var query = datastore.createQuery(kindName2);
    var employees;
    datastore.runQuery(query)
        .then(function (results) {
        employees = results[0];
        console.log(employees);
        return res.status(200).send(employees);
    })["catch"](function (error) {
        console.error('ERROR: ', error);
        return res.status(200).send(error);
    });
}
exports.getEmployees = getEmployees;
;
